#include "udf.h"


#define E 663970
#define R 99.0753 
#define A 4882.88  
#define B -0.000373609
#define S 0.005435
#define Rho 971

/* Define the mass flux profile based on temperature */
DEFINE_PROFILE(mass_flux_profile, thread, index)
{
    face_t f;
    real temp;
    real mass_flux;

    begin_f_loop(f, thread)
    {
        temp = F_T(f, thread);  // Get the temperature at the face
        mass_flux = (exp(log(A)-(E/R)*(1/temp)))*0.001*S*Rho;  // Calculate the mass flux
        F_PROFILE(f, thread, index) = mass_flux;  // Set the mass flux
    }
    end_f_loop(f, thread)
}


/* Define the coefficients 'a' and 'b' */
